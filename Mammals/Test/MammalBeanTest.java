import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;

public class MammalBeanTest {
    MammalBean cat = new MammalBean(3, "Black", 18);
    MammalBean pig = new MammalBean(4, "Pink", 36);
    MammalBean cow = new MammalBean(4, "Brown", 55);
    MammalBean monkey = new MammalBean(2, "Orange", 45);
    MammalBean chicken = new MammalBean(2, "White", 24);
    Set<MammalBean> mammals = new HashSet<>();

    DogBean dog1 = new DogBean("Beagle", "Snoopy", 4, "Brown", 20);
    DogBean dog2 = new DogBean("Labrador", "Skip", 4, "White", 34);
    DogBean dog3 = new DogBean("Pug", "Spot", 4, "Tan", 16);
    DogBean dog4 = new DogBean("Husky", "Shaggy", 4, "Grey", 32);
    DogBean dog5 = new DogBean("Poodle", "Sammy", 4, "Black", 24);

    Map<String, DogBean> dogMap = new TreeMap<>();

    //Object attribute tests:
    @Test
    public void checkLegCount(){
        //MammalBean:
        assertEquals(3, cat.getLegCount());
        assertEquals(4, pig.getLegCount());
        assertEquals(4, cow.getLegCount());
        assertEquals(2, monkey.getLegCount());
        assertEquals(2, chicken.getLegCount());
        //DogBean:
        assertEquals(4, dog1.getLegCount());
        assertEquals(4, dog2.getLegCount());
        assertEquals(4, dog3.getLegCount());
        assertEquals(4, dog4.getLegCount());
        assertEquals(4, dog5.getLegCount());
    }

    @Test
    public void checkColor(){
        //MammalBean:
        assertEquals("Black", cat.getColor());
        assertEquals("Pink", pig.getColor());
        assertEquals("Brown", cow.getColor());
        assertEquals("Orange", monkey.getColor());
        assertEquals("White", chicken.getColor());
        //DogBean:
        assertEquals("Brown", dog1.getColor());
        assertEquals("White", dog2.getColor());
        assertEquals("Tan", dog3.getColor());
        assertEquals("Grey", dog4.getColor());
        assertEquals("Black", dog5.getColor());
    }

    @Test
    public void checkHeight(){
        double x = .00001;
        assertEquals(18, cat.getHeight(), x);
        assertEquals(36, pig.getHeight(), x);
        assertEquals(55, cow.getHeight(), x);
        assertEquals(45, monkey.getHeight(), x);
        assertEquals(24, chicken.getHeight(), x);

        assertEquals(20, dog1.getHeight(), x);
        assertEquals(34, dog2.getHeight(), x);
        assertEquals(16, dog3.getHeight(), x);
        assertEquals(32, dog4.getHeight(), x);
        assertEquals(24, dog5.getHeight(), x);

    }

    @Test
    public void checkBreed() {
        assertEquals("Beagle", dog1.getBreed());
        assertEquals("Labrador", dog2.getBreed());
        assertEquals("Pug", dog3.getBreed());
        assertEquals("Husky", dog4.getBreed());
        assertEquals("Poodle", dog5.getBreed());
    }

    @Test
    public void checkName() {
        assertEquals("Snoopy", dog1.getName());
        assertEquals("Skip", dog2.getName());
        assertEquals("Spot", dog3.getName());
        assertEquals("Shaggy", dog4.getName());
        assertEquals("Sammy", dog5.getName());
    }

    //MammalBean tests:
    @Test
    public void checkOriginalSet() {
        originalSet();
        assertFalse(mammals.contains(chicken));
        assertTrue(mammals.contains(cat));
        assertTrue(mammals.contains(pig));
        assertTrue(mammals.contains(cow));
        assertTrue(mammals.contains(monkey));
    }

    @Test
    public void testNewSet() {
        originalSet();
        mammals.remove(cat);
        mammals.remove(monkey);
        mammals.remove(cow);
        mammals.add(chicken);

        assertFalse(mammals.contains(cat));
        assertFalse(mammals.contains(cow));
        assertFalse(mammals.contains(monkey));
        assertTrue(mammals.contains(pig));
        assertTrue(mammals.contains(chicken));
    }

    public void originalSet() {
        mammals.add(cat);
        mammals.add(pig);
        mammals.add(cow);
        mammals.add(monkey);
    }

    //DogBean tests:
    public void originalMap() {
        dogMap.put(dog1.getName(), dog1);
        dogMap.put(dog2.getName(), dog2);
        dogMap.put(dog3.getName(), dog3);
        dogMap.put(dog4.getName(), dog4);
    }

    @Test
    public void testOriginalMap() {
        originalMap();

        assertFalse(dogMap.containsKey(dog5.getName()));
        assertTrue(dogMap.containsKey(dog1.getName()));
        assertTrue(dogMap.containsKey(dog2.getName()));
        assertTrue(dogMap.containsKey(dog3.getName()));
        assertTrue(dogMap.containsKey(dog4.getName()));
    }

    @Test
    public void testNewMap(){
        originalMap();

        dogMap.put(dog5.getName(), dog5);
        assertTrue(dogMap.containsKey(dog5.getName()));
        assertTrue(dogMap.containsKey(dog1.getName()));
        assertTrue(dogMap.containsKey(dog2.getName()));
        assertTrue(dogMap.containsKey(dog3.getName()));
        assertTrue(dogMap.containsKey(dog4.getName()));

        dogMap.remove(dog1.getName());
        dogMap.remove(dog2.getName());
        dogMap.remove(dog3.getName());
        assertFalse(dogMap.containsKey(dog1.getName()));
        assertFalse(dogMap.containsKey(dog2.getName()));
        assertFalse(dogMap.containsKey(dog3.getName()));
        assertTrue(dogMap.containsKey(dog4.getName()));
        assertTrue(dogMap.containsKey(dog5.getName()));
    }


}
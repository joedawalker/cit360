public class Kennel{
    public static DogBean[] buildDogs() {
        DogBean dog1 = new DogBean("Beagle", "Snoopy", 4, "Brown", 20);
        DogBean dog2 = new DogBean("Labrador", "Skip", 4, "White", 34);
        DogBean dog3 = new DogBean("Pug", "Spot", 4, "Tan", 16);
        DogBean dog4 = new DogBean("Husky", "Shaggy", 4, "Grey", 32);
        DogBean dog5 = new DogBean("Poodle", "Sammy", 4, "Black", 24);

        DogBean[] dogs = new DogBean[5];
        dogs[0] = dog1;
        dogs[1] = dog2;
        dogs[2] = dog3;
        dogs[3] = dog4;
        dogs[4] = dog5;

        return dogs;
    }

    public static void displayDogs(DogBean[] allDogs) {
        for (int i = 0; i < 5; i++) {
            System.out.println(allDogs[i].toString() + "\n");
        }
    }

    public static void main(String[] args) {
        DogBean[] allDogs = buildDogs();
        displayDogs(allDogs);
    }
}

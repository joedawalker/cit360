public class DogBean extends MammalBean {
    private String breed;
    private String name;
    

    DogBean(String breed, String name, int legCount, String color, double height) {
        this.breed = breed;
        this.name = name;
        setLegCount(legCount);
        setColor(color);
        setHeight(height);

    }

    public String getBreed(){
        return breed;
    }

    public void setBreed(){
        this.breed = breed;
    }

    public String getName(){
        return name;
    }

    public void setName(){
        this.name = name;
    }

    public String toString() {
        return "Name: " + this.getName() + "\nBreed: " + this.getBreed() + "\nLeg Count: " + this.getLegCount() + "\nColor: " + this.getColor() + "\nHeight: " + this.getHeight() +"inches";
    }
}

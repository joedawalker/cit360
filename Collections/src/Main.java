import java.util.*;
import java.io.*;
public class Main {

    public static void main(String[] args) {
        File file = new File("rawData.csv");
        TreeMap<String, Account> accountData = new TreeMap<>();

        try {
            String[] read = readText(file);

            // Print the text that was read from the text file.
            for (String line : read) {
                String[] array = splitString(line);
                
                Account temp = new Account(array[0], array[1], array[2], array[3]);
                accountData.put(array[0], temp);
                //System.out.println(line);
            }
            for (Map.Entry<String, Account> entry : accountData.entrySet()) {
                System.out.println(entry.getValue().toString());
            }
        } catch (IOException s) {
            System.out.println("Error reading file | Verify the file location");
        }
    }


    public static String[] readText(File file) throws IOException {
        // Create an ArrayList to hold the lines of text from the file.
        ArrayList<String> text = new ArrayList<>();

        BufferedReader br = null;
        FileReader fr = new FileReader(file);
        try {
            br = new BufferedReader(fr);

            // Read each line from the file and add each line to the ArrayList.
            String line;
            while ((line = br.readLine()) != null) {
                text.add(line);
            }
        } finally {
            // Close the outermost reader that was successfully opened.
            if (br != null) {
                br.close();
            } else {
                fr.close();
            }
        }

        // Create an array large enough to hold all the lines of text
        // from the text file. Copy the text from the ArrayList into the
        // array and return the array.
        int size = text.size();
        String[] array = new String[size];
        return text.toArray(array);
    }

    public static String[] splitString(String s) {
        String[] lines = new String[16];
        int i = 0;
        for (String field : s.split(",")) {
            lines[i] = field;
            i++;
        }
        return lines;
    }
}

